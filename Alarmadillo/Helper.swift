//
//  Helper.swift
//  Alarmadillo
//
//  Created by Danni Brito on 7/23/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import Foundation

struct Helper {
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
